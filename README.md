# Recipe app API proxy 

NGINX proxy app for our ecipe app API

## Usage

### Environment Variables

* 'LISTEN_PORT' - Port to listen on (default: '8080')
* 'APP_HOST' - Hostname of the app to forward requests to (default: 'app')
* 'APP_PORT' - Port of the app to forward requests to (default: '9000')